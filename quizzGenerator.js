export const createChoices = (countries) => {
    const choices = []
    //TODO
    return choices
}

export const createQuizz = (countries) => {
    const quizz = { questions: [], currentQuestion:0 }
    //TODO
    return quizz
}

export const createQuestion = (choices) => {
    choices = [
        {"flags":{"png":"https://flagcdn.com/w320/mh.png","svg":"https://flagcdn.com/mh.svg","alt":"The flag of Marshall Islands has a blue field with two broadening adjacent diagonal bands of orange and white that extend from the lower hoist-side corner to the upper fly-side corner of the field. A large white star with twenty-four rays — four large rays at the cardinal points and twenty smaller rays — is situated in the upper hoist-side corner above the diagonal bands."},"name":{"common":"Marshall Islands","official":"Republic of the Marshall Islands","nativeName":{"eng":{"official":"Republic of the Marshall Islands","common":"Marshall Islands"},"mah":{"official":"Republic of the Marshall Islands","common":"M̧ajeļ"}}},"capital":["Majuro"]},
        {"flags":{"png":"https://flagcdn.com/w320/fi.png","svg":"https://flagcdn.com/fi.svg","alt":"The flag of Finland has a white field with a large blue cross that extend to the edges of the field. The vertical part of this cross is offset towards the hoist side."},"name":{"common":"Finland","official":"Republic of Finland","nativeName":{"fin":{"official":"Suomen tasavalta","common":"Suomi"},"swe":{"official":"Republiken Finland","common":"Finland"}}},"capital":["Helsinki"]},
        {"flags":{"png":"https://flagcdn.com/w320/by.png","svg":"https://flagcdn.com/by.svg","alt":"The flag of Belarus features a vertical band, with a white and red ornamental pattern, spanning about one-fifth the width of the field on the hoist side. Adjoining the vertical band are two horizontal bands of red and green, with the red band twice the height of the green band."},"name":{"common":"Belarus","official":"Republic of Belarus","nativeName":{"bel":{"official":"Рэспубліка Беларусь","common":"Белару́сь"},"rus":{"official":"Республика Беларусь","common":"Беларусь"}}},"capital":["Minsk"]},
        {"flags":{"png":"https://flagcdn.com/w320/mt.png","svg":"https://flagcdn.com/mt.svg","alt":"The flag of Malta is composed of two equal vertical bands of white and red. A representation of the George cross edged in red is situated on the upper hoist-side corner of the white band."},"name":{"common":"Malta","official":"Republic of Malta","nativeName":{"eng":{"official":"Republic of Malta","common":"Malta"},"mlt":{"official":"Repubblika ta ' Malta","common":"Malta"}}},"capital":["Valletta"]},
    ]
    const types = [
        { tip: "flag", answer: "country" },
        { tip: "country", answer: "flag" },
        { tip: "capital", answer: "country" },
        { tip: "country", answer: "capital" }
    ]
    const type = types[Math.floor(Math.random()*4)]
    const question = {
        type: type,
        choices: choices.map((c) => c[type.answer])
    }
    //TODO
    return question
}
