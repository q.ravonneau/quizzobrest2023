import { expect } from "chai";
import { loadCountries } from "../countries.js";
import { createQuestion, createQuizz } from "../quizzGenerator.js";
import { answerQuestion, setGuessType } from "../quizz.js";

describe('quizzGenerator Tests', () => {

    it('Should create 20 questions from countries data', async () => {
        const countries = await loadCountries()
        const quizz = createQuizz(countries)
        expect(quizz).to.be.an("Object")
        expect(quizz.questions).to.be.ok
        expect(quizz.questions).to.be.an("Array")
        expect(quizz.questions.length).to.eql(20)
        for (const question of quizz.questions) {
            expect(question.type.tip).to.be.ok
            expect(question.type.tip).to.be.oneOf(["capital", "country", "flag"])
            expect(question.type.answer).to.be.ok
            expect(question.type.answer).to.be.oneOf(["capital", "country", "flag"])
            expect(question.tip).to.be.ok
            expect(question.tip).to.not.be.empty
            expect(question.answer).to.be.ok
            expect(question.answer).to.not.be.empty
            expect(question.choices).to.be.ok
            expect(question.choices).to.be.an("Array")
            expect(question.choices.length).to.eql(4)
            for (let i = 0; i < 4; i++) {
                const choice = question.choices[i]
                expect(choice).to.be.ok
                expect(choice).to.not.be.empty
            }
            expect(question.answer).to.be.oneOf(question.choices)
        }
        expect(quizz.currentQuestion).to.be.a("Number")
        expect(quizz.currentQuestion).to.eql(0)
        expect(quizz.questions[quizz.currentQuestion].guessType).to.be.null
        expect(quizz.questions[quizz.currentQuestion].guess).to.be.null
    });

    it('Should go forward when current question is answered', async () => {
        const countries = await loadCountries()
        const quizz = createQuizz(countries)
        setGuessType(quizz, "cash")
        const randomChoice = quizz.questions[quizz.currentQuestion].choices[Math.floor(Math.random() * 4)]
        answerQuestion(quizz, randomChoice)
        expect(quizz.currentQuestion).to.eql(1)
        expect(quizz.questions[quizz.currentQuestion - 1].guessType).to.be.ok
        expect(quizz.questions[quizz.currentQuestion - 1].guessType).to.eql("cash")
        expect(quizz.questions[quizz.currentQuestion - 1].guess).to.eql(randomChoice)
    })

    it('Should create one question from 4 countries data', async () => {
        const countries = await loadCountries()
        const question = createQuestion(countries.slice(0, 4))
        expect(question.type.tip).to.be.ok
        expect(question.type.tip).to.be.oneOf(["capital", "country", "flag"])
        expect(question.type.answer).to.be.ok
        expect(question.type.answer).to.be.oneOf(["capital", "country", "flag"])
        expect(question.tip).to.be.ok
        expect(question.tip).to.not.be.empty
        expect(question.answer).to.be.ok
        expect(question.answer).to.not.be.empty
        expect(question.choices).to.be.ok
        expect(question.choices).to.be.an("Array")
        expect(question.choices.length).to.eql(4)
        for (let i = 0; i < 4; i++) {
            const choice = question.choices[i]
            expect(choice).to.be.ok
            expect(choice).to.not.be.empty
        }
        expect(question.answer).to.be.oneOf(question.choices)
    });
});