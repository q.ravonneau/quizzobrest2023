# QuizzoBrest2023

## Consignes

>Créer un jeu de type quizz:  
On veut afficher successivement 20 questions QCM:
Soit on affiche un drapeau et on doit trouver le pays associé.
Soit on affiche un pays et on doit trouver le drapeau associé.
Soit on affiche une capitale et on doit trouver le pays associé.
Soit on affiche un pays et on doit trouver la capitale associé.  
Pour chaque question, l'utilisateur choisit d'abord de répondre librement (5 points), soit parmi quatre propositions (3 points), soit parmi deux propositions (1 point).
En cas de réponse libre, la réponse est validée si elle est similaire à 90% à celle attendue (on pourra utiliser une librairie existante, par ex en JS: https://www.npmjs.com/package/string-similarity).
A la fin des 20 questions, le score est affiché à l'écran.  
On propose à l'utilisateur de rejouer.
On mémorise également les 3 meilleurs scores (on demande alors au joueur un pseudo)
Les données des pays sont fournies dans un fichier JSON, extrait de l'API restcountries.com.

## Outils utilisés

[Git-flow](https://github.com/danielkummer/git-flow-cheatsheet)

[Volta ](https://volta.sh/)

[ExpressJS](https://expressjs.com/fr/)

[EJS](https://ejs.co/)

[Chai](https://www.chaijs.com/)

[Mocha](https://mochajs.org/)